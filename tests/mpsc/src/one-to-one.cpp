#include <channels/mpsc.hpp>

#include <vector>
#include <algorithm>
#include <string>
#include <set>
#include <thread>
#include <chrono>

namespace mpsc = channels::mpsc;

auto test_one_to_one_simple() {
    std::vector<int> values{ 4, -1, 1024, 3, 7, 2 };

    auto [tx, rx] = mpsc::create<int>();

    std::thread t([values](mpsc::Producer<int> tx) {
        for (const auto& val : values) {
            tx.send(val);
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
        }
        tx.send(0);
    }, tx);

    for (std::optional<int> i = rx.receive(), j = 0; *i; ++(*j)) {
        if (*i != values[*j]) {
            t.join();
            return 1;
        }
        i = rx.receive();
    }

    t.join();
    return 0;
}

auto test_one_to_one_complex() {
    std::vector<std::string> values{
        "",
        "small string",
        "very, very long and complicated string"
    };

    auto [tx, rx] = mpsc::create<std::string>();

    std::thread t([values](mpsc::Producer<std::string> tx) {
        for (const auto& val : values) {
            tx.send(val);
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
        }
    }, std::move(tx));

    int res = 0;

    std::set<std::string> seen;
    for (std::optional<std::string> i = rx.receive(); i || rx.do_producers_exist(); i = rx.receive()) {
        if (!i)
            continue;
        if (!seen.count(*i) && std::count(begin(values), end(values), *i)) {
            seen.insert(*i);
            continue;
        }
        res = 1;
        break;
    }

    t.join();

    return res;
}

auto main() -> int {
    return test_one_to_one_simple() + test_one_to_one_complex();
}
