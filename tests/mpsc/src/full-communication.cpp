#include <channels/mpsc.hpp>

#include <unordered_map>
#include <vector>
#include <array>
#include <algorithm>
#include <set>
#include <thread>

namespace mpsc = channels::mpsc;

auto main() -> int {
    std::unordered_map<int, std::vector<int>> data {
        { 0, { 1, 2, 3, 4, 5 } },
        { -1, { 6, 7, 8, 9, 10 } },
        { -2, { 11, 12, 13, 14, 15 } }
    };

    const auto func = [&data](int idx, mpsc::Producer<int> tx) {
        for (const auto v : data[-idx])
            tx.send(v);
        tx.send(idx);
    };

    auto [tx, rx] = mpsc::create<int>();

    std::thread t1 { func, 0, tx };
    std::thread t2 { func, -1, tx };
    std::thread t3 { func, -2, tx };

    std::set<int> received;
    while (true) {
        std::array found {
            received.find(0),
            received.find(-1),
            received.find(-2)
        };

        if (std::all_of(begin(found), end(found), [last = end(received)](const auto x) { return x != last; }))
            break;

        auto v = *rx.receive();
        received.insert(v);
    }

    t1.join();
    t2.join();
    t3.join();

    for (const auto val : received) {
        if (val <= 0) continue;

        bool found = false;
        for (const auto [key, vec] : data)
            for (const auto i : vec)
                if (i == val)
                    found = true;

        if (!found)
            return 1;
    }

    return 0;
}
