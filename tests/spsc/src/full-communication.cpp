#include <channels/spsc.hpp>

#include <vector>
#include <algorithm>
#include <string>
#include <set>
#include <thread>
#include <chrono>

namespace spsc = channels::spsc;

auto main() -> int {
    std::vector<int> values{ 4, -1, 1024, 3, 7, 2 };

    auto [tx, rx] = spsc::create<int>();

    std::thread t([values](spsc::Producer<int> tx) {
        for (const auto& val : values) {
            tx.send(val);
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
        }
        tx.send(0);
    }, std::move(tx));

    for (std::optional<int> i = rx.receive(), j = 0; *i; ++(*j)) {
        if (*i != values[*j]) {
            t.join();
            return 1;
        }
        i = rx.receive();
    }

    t.join();
    return 0;
}
