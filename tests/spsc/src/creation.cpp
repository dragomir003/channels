#include <channels/spsc.hpp>

namespace spsc = channels::spsc;

template <typename T>
auto test_creation() -> int {

    auto [tx, rx] = spsc::create<T>();

    return !(tx.is_valid() && rx.is_valid());
}

#include <vector>
#include <string>
#include <unordered_map>
#include <utility>
#include <array>
#include <numeric>


auto main() -> int {
    std::array results{
        test_creation<int>(),
        test_creation<bool>(),
        test_creation<float>(),
        test_creation<size_t>(),
        test_creation<std::string>(),
        test_creation<std::vector<char>>(),
        test_creation<std::tuple<int, std::string, bool>>(),
        test_creation<std::vector<std::pair<int, std::unordered_map<std::string, float>>>>(),
    };

    return std::reduce(begin(results), end(results));
}
